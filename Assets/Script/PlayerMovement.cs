using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float MoveSpeed;
    public float Jumpforce;

    private bool isJumping;
    private bool isGrounded;

    public Rigidbody2D rb;
    private Vector3 Velocity = Vector3.zero;

    public Transform GroundCheckLeft;
    public Transform GroundCheckRight;
    public Animator animator;
    public SpriteRenderer spriteRenderer;
    

    void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapArea(GroundCheckLeft.position, GroundCheckRight.position);
        float horizontalMovement = Input.GetAxis("Horizontal") * MoveSpeed * Time.deltaTime;

        if (Input.GetButton("Jump") && isGrounded)
        {
            isJumping = true;
        }
        MovePlayer(horizontalMovement);


        Flip(rb.velocity.x);

        float characterVelocity = Mathf.Abs(rb.velocity.x);
        
        animator.SetFloat("Speed", characterVelocity);

    }

    void MovePlayer(float _horizontalMovenment)
    {
        Vector3 targetVelocity = new Vector2(_horizontalMovenment, rb.velocity.y);
        rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocity, ref Velocity, 0.05f);
        if(isJumping == true)
        {
            rb.AddForce(new Vector2(0f, Jumpforce));
            isJumping = false;
        }
    }

    void Flip(float _velocity)
    {
        if (_velocity > 0.1f)
        {
            spriteRenderer.flipX = false;
        }
        else if(_velocity < -0.1f)
        {
            spriteRenderer.flipX = true;
        }
    }
}
