using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EncountCheck : MonoBehaviour
{

    public int index;
    public string levelName;
    void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject collisionGameObject = collision.gameObject;

        if (collision.CompareTag("Player"))
        {
            SceneManager.LoadScene(index);
            //SceneManager.LoadScene("BattleMap");
        }
    }

}